﻿using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour {

	[SerializeField] private Transform target;
	public float rotSpeed = 1.5f;
	private float _rotX;
	private float _rotY;

	private Vector3 _offset;
	void Start() {
		_rotX = transform.eulerAngles.x;
		_rotY = transform.eulerAngles.y;
		_offset = target.position - transform.position;
	}
	void LateUpdate() {


		_rotY += Input.GetAxis("Mouse X") * rotSpeed * 3;


		_rotX -= Input.GetAxis ("Mouse Y") * rotSpeed * 3;

		_rotX = Mathf.Clamp (_rotX, -60, 85);
		Quaternion rotation = Quaternion.Euler(_rotX, _rotY, 0);
		transform.position = target.position - (rotation * _offset);
		transform.LookAt(target);
	}



}
