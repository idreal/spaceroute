﻿using UnityEngine;
using System.Collections;
using System;

public class RocketControlls : MonoBehaviour {

	public float speed = 8f;

	[SerializeField] private AudioSource soundSource;
	[SerializeField] private AudioClip splitSound;

	private Rigidbody _rigidbody;

	bool hasSoundPlayed;

	[SerializeField] private GameObject _particle;

	private float _fuel = 100;

	// Use this for initialization
	void Start () {
		_rigidbody = GetComponent<Rigidbody> ();
	}

	void Update()
	{
		if(transform.position.y > 700)
		{
			StartCoroutine (LoadLevelCoroutine ());
		}
	}
	IEnumerator LoadLevelCoroutine()
	{
		if (!hasSoundPlayed) {
			soundSource.PlayOneShot (splitSound);
			hasSoundPlayed = true;
		}
		yield return new WaitForSeconds (2);
		Application.LoadLevel ("Scene2");
	}

	// Update is called once per frame
	void FixedUpdate () {

		//_rigidbody.velocity = new Vector3 (0, movementY * speed, 0);
		//_rigidbody.AddForce (Physics.gravity);


		if(Input.GetButton("D"))
		{
			float rotationY = transform.eulerAngles.y;
			rotationY -= 1;
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x, rotationY, transform.eulerAngles.z);
		}
		else if(Input.GetButton("A"))
		{
			float rotationY = transform.eulerAngles.y;
			rotationY += 1;
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x, rotationY,transform.eulerAngles.z);
		}

		if(Input.GetButton("S"))
		{
			float rotationX = transform.eulerAngles.x;
			rotationX -= 1;
			transform.eulerAngles = new Vector3 (rotationX, transform.eulerAngles.y, transform.eulerAngles.z);
		}
		else if(Input.GetButton("W"))
		{
			float rotationX = transform.eulerAngles.x;
			rotationX += 1;
			transform.eulerAngles = new Vector3 (rotationX, transform.eulerAngles.y, transform.eulerAngles.z);
		}

		if(Input.GetButton("E"))
		{
			float rotationZ = transform.eulerAngles.z;
			rotationZ -= 1;
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x, transform.eulerAngles.y, rotationZ);
		}
		else if(Input.GetButton("Q"))
		{
			float rotationZ = transform.eulerAngles.z;
			rotationZ += 1;
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x, transform.eulerAngles.y, rotationZ);
		}



		//_rigidbody.AddForce (new Vector3 (0, movementY * speed, 0));
		if(Input.GetButton("Jump"))
		{
			_particle.SetActive (true);
			_rigidbody.AddRelativeForce (new Vector3 (0, speed, 0));
			_fuel -= 0.02f;
			_rigidbody.mass -= 0.0002f;
		}
		else{
			_particle.SetActive (false);
		}

	}

	void OnGUI()
	{
		GUI.Label (new Rect (10, 10, 400, 100), "Fuel: " + _fuel.ToString() + "   Altitude: " + transform.position.y.ToString() + "  Speed: " + Convert.ToInt32(_rigidbody.velocity.y).ToString());
	}
		
}
