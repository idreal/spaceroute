﻿using UnityEngine;
using System.Collections;

public class InteractiveObject : MonoBehaviour {

	public GameObject rocket;
	public GameObject _camera;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay(Collider col)
	{
		if(col.tag == "Player")
		{
			if(Input.GetButtonDown("Fire3"))
			{
				//change color
				//Debug.Log ("Interacted");
				Color random = new Color(Random.Range(0f,1f),
					Random.Range(0f,1f), Random.Range(0f,1f));
				GetComponent<Renderer>().material.color = random;
				StartCoroutine (LaunchRocket ());
			}
		}
	}

	IEnumerator LaunchRocket()
	{
		yield return new WaitForSeconds (1f);

		_camera.transform.parent = null;
		DestroyObject (GameObject.FindGameObjectWithTag ("Player"));
		_camera.GetComponent<MouseLook> ().enabled = false;
		_camera.GetComponent<OrbitCamera> ().enabled = true;
		rocket.GetComponent<Rigidbody> ().isKinematic = false;
		rocket.GetComponent<RocketControlls> ().enabled = true;
		rocket.GetComponent<AudioSource> ().enabled = true;
		_camera.transform.position = rocket.transform.position;
		_camera.transform.parent = rocket.transform;
		_camera.transform.position = new Vector3 (_camera.transform.position.x, _camera.transform.position.y, _camera.transform.position.z - 40);

	}
}
